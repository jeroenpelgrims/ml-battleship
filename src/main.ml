open Core.Std

type screens =
  | MainMenu

let read_screen screen =
  let filename = match screen with
    | MainMenu -> "src/screens/main_menu.txt"
  in
    In_channel.read_all filename

let print_screen screen =
  read_screen screen
  |> printf "%s"

type main_menu_choice =
  | NewGame
  | Highscore

let rec main_menu () =
  begin
    print_screen MainMenu;
    match In_channel.input_line stdin with
    | Some "1" -> NewGame
    | Some "2" -> Highscore
    | Some _ | None -> main_menu ()
  end

let () = match main_menu () with
  | NewGame ->
    printf "\n%s" "user chose new game"
  | Highscore ->
    printf "\n%s" "user chose High score"
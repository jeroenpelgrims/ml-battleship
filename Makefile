# Makefile
# ocamlbuild -use-ocamlfind -pkg core -tag thread main.d.byte

dist:
	corebuild src/main.native
build:
	corebuild src/main.d.byte
clean:
	ocamlbuild -clean
.PHONY: build clean